

// make a matriz of 3x3
const matriz = [
    ['0 - 0', '0 - 1', '0 - 2', '0 - 3', '0 - 4', '0 - 5'],
    ['1 - 0', '1 - 1', '1 - 2', '1 - 3', '1 - 4', '1 - 5'],
    ['2 - 0', '2 - 1', '2 - 2', '2 - 3', '2 - 4', '2 - 5'],
    ['3 - 0', '3 - 1', '3 - 2', '3 - 3', '3 - 4', '3 - 5'],
    ['4 - 0', '4 - 1', '4 - 2', '4 - 3', '4 - 4', '4 - 5'],
    ['5 - 0', '5 - 1', '5 - 2', '5 - 3', '5 - 4', '5 - 5'],
];

let visitados = [];

let ruta = [];

Array.prototype.contains = function (element) {
    return this.indexOf(element) > -1;
}
Array.prototype.remove = function (element) {
    let index = this.indexOf(element);
    if (index > -1) {
        this.splice(index, 1);
    }
}
const expandir = (origen) => {
    let hijos = [];

    let x = parseInt(origen.split(' - ')[0]);
    // console.log('x: ', x);
    let y = parseInt(origen.split(' - ')[1]);
    // console.log('y: ', y);
    if ( y < matriz[x].length - 1 && matriz[x][y + 1] !== undefined && !visitados.contains(matriz[x][y + 1]) ) { 
        hijos.push(matriz[x][y + 1]);
    }
    if ( y != 0 && matriz[x][y - 1] !== undefined && !visitados.contains(matriz[x][y - 1]) ) {
        hijos.push(matriz[x][y - 1]);
    }
    if ( x != 0 && matriz[x - 1][y] !== undefined && !visitados.contains(matriz[x - 1][y]) ) {
        hijos.push(matriz[x - 1][y]);
    }
    if ( x < matriz.length - 1 && matriz[x + 1][y] !== undefined && !visitados.contains(matriz[x + 1][y]) ) {
        hijos.push(matriz[x + 1][y]);
    }
    
    return hijos;
}

const bpp = (visitado, grafo, origen, destino, meta) => {

    if (origen === destino) {
        ruta.push(origen);
        return 1;
    }
    else if (meta === 0 || meta == 2) {
        let hijos = expandir(origen);
        // la rama no es solución
        if (hijos.length === 0) {
            ruta.remove(origen);
            return 2;
        }
        else if (!visitado.contains(origen)) {
            visitado.push(origen);
            ruta.push(origen);
            for (let hijo of hijos) {
                ruta.push(hijo);
                meta = bpp(visitado, grafo, hijo, destino, meta);
                if (meta === 1) {
                    ruta.remove(hijo);
                    return 1;
                }
                ruta.remove(hijo);
            }
            if (visitado.contains(origen)) {
                ruta.remove(origen);
                return 2;
            }
        }
    }
    return 0;
}



bpp(visitados, matriz, '2 - 3', '3 - 1', 0);
console.log('Ruta: ', ruta);


